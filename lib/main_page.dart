import 'package:column_widget_example/navigation_drawer_widget.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) => Scaffold(
drawer: NavigationDrawerWidget(),
    appBar: AppBar(
      backgroundColor: Colors.lightBlueAccent,
    ),
  );
}
