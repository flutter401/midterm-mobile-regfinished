import 'package:flutter/material.dart';

class NavigationDrawerWidget extends StatelessWidget{
  final padding = EdgeInsets.symmetric(horizontal: 20);
  @override
  Widget build(BuildContext context){
    final safeArea = EdgeInsets.only(top: MediaQuery.of(context).viewPadding.top);
    final isCollapsed = true;

    return Container(
      width: isCollapsed ? MediaQuery.of(context).size.width*0.5:null,
      child: Drawer(
        child: Container(
          color: Color(0xFF1a2f45),
          child: Column(
            children: [
              Container(
                padding: safeArea,
                child: buildHeader(isCollapsed),
              ),
            ],
          ),
        ),
      ),
    );
    //return Drawer();
  }

  Widget buildHeader(bool isCollapsed) =>Row(
    children: [
      const SizedBox(width: 24),
      FlutterLogo(size: 48),
      const SizedBox(width: 16),
      Text('Menu',
      style: TextStyle(fontSize: 32, color: Colors.white),
      ),
    ],
  );
}

